﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Xml;
using System;

public class BuildProcessor : MonoBehaviour
{

    [MenuItem("Build/Custom Build", false, 0)]
    static void CustomBuildOnly()
    {
        CustomBuild(false);
    }

    //[MenuItem("Build/Custom Build - Run", false, 1)]
    //static void CustomBuildRun()
    //{
    //    CustomBuild(true);
    //}

    static void CustomBuild(bool runbuild)
    {
        //BuildOptions customBuildOptions = EditorUserBuildSettings.activeBuildTarget;
        BuildOptions customBuildOptions = BuildOptions.ShowBuiltPlayer;
        if (runbuild)
        {
            //set EditorUserBuildSettings.architectureFlags enum flags set buildrun bit on
            if ((customBuildOptions & BuildOptions.AutoRunPlayer) != BuildOptions.AutoRunPlayer)
            {
                customBuildOptions = customBuildOptions | BuildOptions.AutoRunPlayer;
            }
        }
        else
        {
            //set EditorUserBuildSettings.architectureFlags enum flags set showfolder bit on
            if ((customBuildOptions & BuildOptions.ShowBuiltPlayer) != BuildOptions.ShowBuiltPlayer)
            {
                customBuildOptions = customBuildOptions | BuildOptions.ShowBuiltPlayer;
            }
        }
        BuildTarget mycustombuildtarget = EditorUserBuildSettings.activeBuildTarget;
        string extn = "";
        switch (EditorUserBuildSettings.selectedBuildTargetGroup)
        {
            case BuildTargetGroup.Standalone:
                switch (EditorUserBuildSettings.selectedStandaloneTarget)
                {
                    case BuildTarget.StandaloneWindows:
                        mycustombuildtarget = BuildTarget.StandaloneWindows;
                        extn = "exe";
                        break;
                    //	case BuildTarget.StandaloneWindows64:
                    //		mycustombuildtarget=BuildTarget.StandaloneWindows64;
                    //		extn="exe";
                    //		break;
                    case BuildTarget.StandaloneOSXUniversal:
                        mycustombuildtarget = BuildTarget.StandaloneOSXUniversal;
                        extn = "app";
                        break;
                    case BuildTarget.StandaloneOSXIntel:
                        mycustombuildtarget = BuildTarget.StandaloneOSXIntel;
                        extn = "app";
                        break;
                }
                break;
            case BuildTargetGroup.WebPlayer:
                if (EditorUserBuildSettings.webPlayerStreamed)
                {
                    mycustombuildtarget = BuildTarget.WebPlayerStreamed;
                    extn = "unity3d";
                }
                else
                {
                    mycustombuildtarget = BuildTarget.WebPlayer;
                    extn = "unity3d";
                }
                break;
            case BuildTargetGroup.iOS:
                mycustombuildtarget = BuildTarget.iOS;
                extn = "xcode";
                break;
            case BuildTargetGroup.PS3:
                mycustombuildtarget = BuildTarget.PS3;
                //extn="???"
                break;
            case BuildTargetGroup.XBOX360:
                mycustombuildtarget = BuildTarget.XBOX360;
                //extn="???"
                break;
            case BuildTargetGroup.Android:
                mycustombuildtarget = BuildTarget.Android;
                extn = "apk";
                break;
            case BuildTargetGroup.GLESEmu:
                mycustombuildtarget = BuildTarget.StandaloneGLESEmu;
                //extn="???"
                break;
        }

        string savepath = EditorUtility.SaveFilePanel("Build " + mycustombuildtarget,
        EditorUserBuildSettings.GetBuildLocation(mycustombuildtarget), "", extn);
        if (savepath.Length != 0)
        {
            string dir = System.IO.Path.GetDirectoryName(savepath); //get build directory
            string[] scenes = new string[EditorBuildSettings.scenes.Length];
            for (int i = 0; i < EditorBuildSettings.scenes.Length; ++i)
            {
                scenes[i] = EditorBuildSettings.scenes[i].path.ToString();
            };
            PreProcess(dir);
            BuildPipeline.BuildPlayer(scenes, savepath, mycustombuildtarget, customBuildOptions);
            EditorUserBuildSettings.SetBuildLocation(mycustombuildtarget, dir); //store new location for this type of build
            PostProcess(dir); // * warning, if set to build and run in build settings, this may fire after application execution.
        }
        Debug.Log(mycustombuildtarget.ToString());
    }

    static void PreProcess(string BuildPathDir)
    {
        //Your pre-process here, copy Assets/Folders to Directory, edit html pages etc
        //or you could execute a shell, cgi-perl or bat command by doing something like below:-
        //Application.OpenURL("yourfile.bat "+BuildPathDir); //etc

        TextAsset configFile = (TextAsset)Resources.Load("GlobalConfiguration");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(configFile.text);

        PlayerSettings.companyName = xmlDoc.GetElementById("companyName").InnerText;
        PlayerSettings.productName = xmlDoc.GetElementById("productName").InnerText;
        PlayerSettings.bundleIdentifier = xmlDoc.GetElementById("bundleIdentifier").InnerText;
        PlayerSettings.bundleVersion = xmlDoc.GetElementById("bundleVersion").InnerText;
        PlayerSettings.Android.bundleVersionCode = Convert.ToInt32(xmlDoc.GetElementById("bundleVersionCode").InnerText);
        PlayerSettings.keystorePass = xmlDoc.GetElementById("keystorePass").InnerText;
        PlayerSettings.keyaliasPass = xmlDoc.GetElementById("keyAliasPass").InnerText;

    }

    static void PostProcess(string BuildPathDir)
    {
        //Your post-process here, copy Assets/Folders to Directory, edit html pages etc
        //*warning, if set to run after build, these commands may execute after the built application is run.
        //Application.OpenURL("yourfile.bat "+BuildPathDir); //etc
    }
}