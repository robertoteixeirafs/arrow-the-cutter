﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class MenuController : MonoBehaviour 
{

    public Image soundBtnImage;
    public Image soundBtnGameoverImage;
    public Sprite soundOn;
    public Sprite soundOff;
    public Sprite shareImage;

    void Start()
    {
        if (AudioManager.Instance.musicOn)
        {
            AudioManager.Instance.PlayMusicAudio();
            soundBtnImage.sprite = soundOn;
            soundBtnGameoverImage.sprite = soundOn;
        }
        else
        {
            AudioManager.Instance.StopMusicAudio();
            soundBtnImage.sprite = soundOff;
            soundBtnGameoverImage.sprite = soundOff;
        }        
    }

    public void Mute()
    {
        if (AudioManager.Instance.musicOn)
        {
            AudioManager.Instance.StopMusicAudio();
            soundBtnImage.sprite = soundOff;
            soundBtnGameoverImage.sprite = soundOff;
        }
        else
        {
            AudioManager.Instance.PlayButtonEffect();
            AudioManager.Instance.PlayMusicAudio();
            soundBtnImage.sprite = soundOn;
            soundBtnGameoverImage.sprite = soundOn;
        }
    }

    public void Rate()
    {
        AudioManager.Instance.PlayButtonEffect();
        Application.OpenURL("market://details?id=com.om.arrow");
    }

    public void ShareText()
    {
#if UNITY_ANDROID
        var croppedTexture = new Texture2D((int)shareImage.rect.width, (int)shareImage.rect.height);
        var pixels = shareImage.texture.GetPixels((int)shareImage.textureRect.x,
                                        (int)shareImage.textureRect.y,
                                        (int)shareImage.textureRect.width,
                                        (int)shareImage.textureRect.height);
        croppedTexture.SetPixels(pixels);
        croppedTexture.Apply();


        // create the texture
        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        byte[] dataToSave = croppedTexture.EncodeToPNG();
        string destination = Path.Combine(Application.persistentDataPath, System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
        File.WriteAllBytes(destination, dataToSave);

        //string shareText = "This is the text";
        string shareText = "I have been playing \"Arrow-The Cutter\" on my Android and it is awesome. You should check it out. Just click the link below to download or search \"Arrow - The Cutter\" in the Google Play Store! https://play.google.com/store/apps/details?id=com.om.arrow&hl=en";
        string subject = "Arrow - The Cutter";


        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
        intentObject.Call<AndroidJavaObject>("setType", "image/png");

        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
        currentActivity.Call("startActivity", jChooser); 
#endif

    }


}
