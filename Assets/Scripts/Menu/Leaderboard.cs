﻿using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using System.Collections;
using GooglePlayGames.BasicApi;

public class Leaderboard : MonoBehaviour 
{
    //private const string LEADERBOARD_ID = "CgkIn6_MncUDEAIQBg";
    private const string LEADERBOARD_ID = "CgkIlM7i7fUFEAIQAQ";        
     
    void Awake()
    {
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate(); 
#endif
    }

    public void ShowLeaderBoard()
    {
        AudioManager.Instance.PlayButtonEffect();
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) =>
        {
            Debug.Log("Login: " + success);
            if (success)
            {
                long score = PlayerPrefs.GetInt("BestScore");

                Social.ReportScore(score, LEADERBOARD_ID, (bool result) => {
                    PlayGamesPlatform.Instance.ShowLeaderboardUI(LEADERBOARD_ID);
                });                

            }
        }); 
#endif
    }


}
