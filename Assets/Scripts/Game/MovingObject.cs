﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour
{

    public float yPos;    
    public float speed;
    public bool isGreen;

    private Vector3 originalPos;
    //private Animator anim;
    private Collider2D col;

    private Transform objUp;
    private Transform objDown;

    private bool destroy;

    void Awake()
    {        
        originalPos = transform.position;
        //anim = GetComponent<Animator>();
        col = GetComponent<Collider2D>();
        objUp = transform.FindChild("square_up").transform;
        objDown = transform.FindChild("square_down").transform;
    }

    void Update()
    {
        speed = GameController.GameSpeed;

        if (transform.position.x < -4.0f)
            DestroyGameObject();

        if (speed > 0)
        {
            transform.position -= new Vector3(1.4f * speed * Time.deltaTime, 0, 0);
        }

        if(destroy)
        {
            objUp.transform.localScale = Vector3.Lerp(objUp.transform.localScale, new Vector3(0, 0, 0), 1.5f * Time.deltaTime);
            objDown.transform.localScale = Vector3.Lerp(objDown.transform.localScale, new Vector3(0, 0, 0), 1.5f * Time.deltaTime);
            //objUp.transform.localScale -= new Vector3(transform.localScale.x+transform.localScale.x*0.10f, transform.localScale.y+transform.localScale.y*0.10f,0) * Time.deltaTime;
            //objDown.transform.localScale -= new Vector3(transform.localScale.x + transform.localScale.x * 0.10f, transform.localScale.y + transform.localScale.y * 0.10f, 0) * Time.deltaTime;
        }
    }

    void OnBecameInvisible()
    {
        transform.position = originalPos;
        gameObject.SetActive(false);
    }

    public void ReturnToOriginalPos()
    {
        transform.position = originalPos;
        gameObject.SetActive(false);
    }

    public void DestroyGameObject()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (GameController.greenOn == isGreen)
            {
                destroy = true;
                col.enabled = false;
                //anim.SetTrigger("cut");
                GameController.AddScore();
            }
            else
            {
                //gameover
                AudioManager.Instance.StopMainTheme();
                AudioManager.Instance.PlayFailEffect();
                GameController.GameOver = true;
                //Destroy(other.gameObject);
                other.enabled = false;
                other.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
            }
        }
    }

}
