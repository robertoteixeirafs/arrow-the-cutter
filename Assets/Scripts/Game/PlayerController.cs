﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
    GameController gameController;

    void Awake()
    {
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
    }

    void Update()
    {
        if (gameObject.transform.position.y < -9)
        {
            YouLose();
            Destroy(gameObject);
        }
    }

    public void YouLose()
    {
        gameController.GameIsOver();
    }

}
