﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GoogleMobileAds.Api;
using System;
using System.Xml;

public class GameController : MonoBehaviour {

    public static float GameSpeed = 1.7f;
    public static bool greenOn = true;
    public static bool GameOver = false;
    public static int gameScore = 0;
    public static int playCount = 0;

    public Sprite green;
    public Sprite blue;
    public GameObject player;
    public Vector3 spawnPoint;

    public string admobBannerId;
    public string admobInterstitialId;
      
    private SpriteRenderer playerSP;
    private Text pointsTxt;//Game panel
    private SpawnController spawnController;
    private Text scoreTxt; //Gameover panel
    private Animator gameoverMenuAnim;
    private Animator gameMenuAnim;
    private bool canTouch = false;
    private bool gameStarted = false;
    
    //Touch to cutting text
    private Text touchTxt;
    private Text toTxt;
    private Text cuttingTxt;

    //Ads
    private InterstitialAd interstitial;
    private InterstitialAd interstitial2;
    
	void Start () 
    {
        playerSP = player.GetComponent<SpriteRenderer>();
        pointsTxt = GameObject.Find("points").GetComponent<Text>();
        scoreTxt = GameObject.Find("scoreTxt").GetComponent<Text>();
        spawnController = GameObject.Find("SpawnController").GetComponent<SpawnController>();
        gameoverMenuAnim = GameObject.Find("GameoverMenu").GetComponent<Animator>();
        gameMenuAnim = GameObject.Find("MainMenu").GetComponent<Animator>();
        touchTxt = GameObject.Find("touch").GetComponent<Text>();
        toTxt = GameObject.Find("to").GetComponent<Text>();
        cuttingTxt = GameObject.Find("cutting").GetComponent<Text>();
        GameObject.Find("bestTxt").GetComponent<Text>().text = PlayerPrefs.GetInt("BestScore").ToString();

        if (PlayerPrefs.GetInt("FirstPlay") == 1)
        {
            GameObject.Find("MainMenu").SetActive(false);
            //Destroy(GameObject.Find("MainMenu"));
            CanTouch();
        }

        LoadAdIds();
        RequestInterstitial();
	}
		
	void Update () 
    {
        
if (canTouch)
	{
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR 
        if (!GameOver)
        {
            pointsTxt.text = gameScore.ToString();
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (!gameStarted)
                    { 
                        StartGame();
                    }
                }

                if (Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    playerSP.sprite = blue;
                    greenOn = false;
                }
                else
                {
                    playerSP.sprite = green;
                    greenOn = true;
                }
            }
            else
            {
                playerSP.sprite = green;
            }  
        }
#endif
#if UNITY_EDITOR
        if (!GameOver)
        {
            pointsTxt.text = gameScore.ToString();
            if (Input.GetMouseButtonDown(0))
            {
                if (!gameStarted)
                {
                    StartGame();
                }

                playerSP.sprite = blue;
                greenOn = false;
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (playerSP)
                {
                    playerSP.sprite = green;
                    greenOn = true; 
                }
            }  
        }
#endif 
	}

	}

    public static void AddScore()
    {
        gameScore++;

        if (gameScore > 12 && gameScore < 30)
        {
            GameSpeed = 1.7f;            
        }
        else if (gameScore > 30 && gameScore < 45)
        {
            GameSpeed = 2.0f;
        }
        else if (gameScore > 45 && gameScore < 70)
        {
            GameSpeed = 2.5f;
        }
        else if (gameScore > 70 && gameScore < 90)
        {
            GameSpeed = 3.0f;
        }
        else if (gameScore > 130)
        {
            GameSpeed += 0.05f;
        }
    }

    public void GameIsOver()
    {
        playCount++;
        scoreTxt.text = gameScore.ToString();

        int bestScore = PlayerPrefs.GetInt("BestScore");
        if (gameScore > bestScore)
        {
            PlayerPrefs.SetInt("BestScore", gameScore);
        }

        if (playCount == 3)
        {
            interstitial.Show();
            playCount = 0;
        }

        gameoverMenuAnim.SetBool("show", true);
    }

    public void StartGame()
    {        
        gameStarted = true;
        spawnController.Spawn();
        touchTxt.text = " ";
        toTxt.text = " ";
        cuttingTxt.text = " ";        
    }

    public void PlayButton()
    {
        AudioManager.Instance.PlayButtonEffect();
        AudioManager.Instance.PlayMainTheme();
        gameMenuAnim.SetTrigger("hide");
        Invoke("CanTouch", 0.5f);
        RequestBanner();
    }

    public void RestartButton()
    {
        AudioManager.Instance.PlayButtonEffect();
        AudioManager.Instance.PlayMainTheme();
        DefaultValues();
        Application.LoadLevel("Game");
        PlayerPrefs.SetInt("FirstPlay", 1);
    }

    void CanTouch()
    {
        canTouch = true;
    }

    void DefaultValues()
    {
        GameSpeed = 1.7f;        
        gameScore = 0;
        greenOn = true;
        GameOver = false;
    }

    public void MoreGames()
    {
        Debug.Log("More Games");
        AudioManager.Instance.PlayButtonEffect();
        interstitial2.Show();
        GameObject.Find("moreGames").SetActive(false);
    }

    #region AdMob

    void LoadAdIds()
    {
        TextAsset configFile = (TextAsset)Resources.Load("GlobalConfiguration");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(configFile.text);

        admobBannerId = xmlDoc.GetElementById("bannerId").InnerText;
        admobInterstitialId = xmlDoc.GetElementById("interstitialId").InnerText;

        Debug.Log("Banner ID - " + admobBannerId);
        Debug.Log("Interstitial ID - " + admobInterstitialId);
    }

    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID        
        //string adUnitId = "ca-app-pub-9901414956520050/4732169321";
        string adUnitId = admobBannerId;
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        BannerView bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        print("OnAdLoaded event received.");
        // Handle the ad loaded event.
    }

    public void HandleOnAdLoadedInterstitial(object sender, EventArgs args)
    {
        print("OnAdLoaded event received - Interstitial.");
        // Handle the ad loaded event.
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("Banner Failed to load: " + args.Message);
        // Handle the ad failed to load event.
    }

    public void HandleOnAdFailedToLoadInterstitial(object sender, AdFailedToLoadEventArgs args)
    {
        print("Interstitial Failed to load: " + args.Message);
        // Handle the ad failed to load event.
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        //string adUnitId = "ca-app-pub-9901414956520050/6208902528";
        string adUnitId = admobInterstitialId;
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        interstitial.OnAdLoaded += HandleOnAdLoadedInterstitial;
        interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoadInterstitial;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);

        interstitial2 = new InterstitialAd(adUnitId);
        interstitial2.OnAdLoaded += HandleOnAdLoadedInterstitial;
        interstitial2.OnAdFailedToLoad += HandleOnAdFailedToLoadInterstitial;
        // Create an empty ad request.
        AdRequest request2 = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial2.LoadAd(request2); 
    }
 
    #endregion AdMob
}
