﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour {

    public List<GameObject> objectsToSpawn;
    public float spawnInterval;
    public Vector3 spawnPoint;	

    public void Spawn()
    {
        if (!GameController.GameOver)
        {
            Instantiate(objectsToSpawn[Random.Range(0, 2)], spawnPoint, Quaternion.identity);            
            //Invoke("Spawn", spawnInterval); 
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Spawn();
    }
}
