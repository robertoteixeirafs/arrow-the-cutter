﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : SingletonAsComponent<AudioManager>
{

    public static AudioManager Instance
    {
        get { return ((AudioManager)_Instance); }
        set { _Instance = value; }
    }


    public AudioSource musicAudioSource;
    public AudioSource effectsAudioSource;
    public AudioSource effectsAudioSource_2;
    public AudioClip mainThemeEffect;
    public AudioClip buttonEffect;
    public AudioClip successEffect;
    public AudioClip failEffect;
    public bool musicOn = true;
    public float maxVolume = 0.5f;
    public float minVolume = 0.1f;

    private bool fadeIn = false;
    private bool fadeOut = false;

    void Awake()
    {
        VerifyAudio();
        DontDestroyOnLoad(this.gameObject);
    }

    void Update()
    {
        if (fadeIn && musicAudioSource.volume <= maxVolume)
        {
            musicAudioSource.volume += 0.1f * Time.deltaTime;
        }
        else
            fadeIn = false;

        if (fadeOut && musicAudioSource.volume >= minVolume)
        {
            musicAudioSource.volume -= 0.1f * Time.deltaTime;
        }
        else
            fadeOut = false;
    }

    public void PlayMainTheme()
    {
        musicAudioSource.clip = mainThemeEffect;
        //VolumeUpMainTheme();
        musicAudioSource.Play();
    }

    public void PlayMainThemeEffect()
    {
        effectsAudioSource.clip = mainThemeEffect;
        //VolumeUpMainTheme();
        effectsAudioSource.Play();
    }

    public void PlayButtonEffect()
    {
        effectsAudioSource.clip = buttonEffect;
        effectsAudioSource.Play();
    }

    public void PlaySuccessEffect()
    {
        effectsAudioSource_2.clip = successEffect;
        effectsAudioSource_2.Play();
    }

    public void PlayFailEffect()
    {
        effectsAudioSource.clip = failEffect;
        effectsAudioSource.Play();
    }

     public void StopMainTheme()
    {
        musicAudioSource.Stop();
    }


    public void VolumeDownMainTheme()
    {
        musicAudioSource.volume = 0.1f;
        fadeIn = false;
        fadeOut = false;
    }

    public void VolumeUpMainTheme()
    {
        musicAudioSource.volume = 1f;
    }


    public void FadeInMusic()
    {
        fadeOut = false;
        fadeIn = true;
    }

    public void FadeOutMusic()
    {
        fadeIn = false;
        fadeOut = true;
    }

    public void PlayMusicAudio()
    {
        musicAudioSource.mute = false;
        effectsAudioSource.mute = false;
        musicOn = true;
        musicAudioSource.volume = 0.5f;
        PlayerPrefs.SetString("audioOn", "true");
        PlayerPrefs.Save();
    }

    public void StopMusicAudio()
    {
        musicAudioSource.mute = true;
        effectsAudioSource.mute = true;
        musicOn = false;
        PlayerPrefs.SetString("audioOn", "false");
        PlayerPrefs.Save();
    }

    public void VolumeDownAll()
    {
        musicAudioSource.volume = 0f;
        effectsAudioSource.volume = 0f;
        effectsAudioSource_2.volume = 0f;
    }

    public void VolumeUpAll()
    {
        musicAudioSource.volume = 0.5f;
        effectsAudioSource.volume = 1f;
        effectsAudioSource_2.volume = 1f;
    }

    public void VerifyAudio()
    {
        if (PlayerPrefs.HasKey("audioOn") && PlayerPrefs.GetString("audioOn") != "")
        {
            if (PlayerPrefs.GetString("audioOn") == "true")
            {
                musicOn = true;
                PlayMusicAudio();
            }
            else
            {
                musicOn = false;
                StopMusicAudio();
            }
        }
        else
        {
            PlayMusicAudio();
            PlayerPrefs.SetString("audioOn", "true");
            PlayerPrefs.Save();
            musicOn = true;
        }
    }
}
